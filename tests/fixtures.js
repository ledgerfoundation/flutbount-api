const { ObjectId } = require('mongodb');

// exports.users = [{
//     _id: ObjectId('5cbd9f1b54aaf839c0ab691f'),
//     profilePic: null,
//     name: 'abc',
//     age: 65,
//     gender: 0,
//     city: 'london',
//     skills: [
//         ObjectId('5cbde37e952db9f5ae6890d7'),
//         ObjectId('5cbde407952db9f5ae689117'),
//     ],
//     languages: [
//         ObjectId('5cbdea09952db9f5ae6892c2'),
//     ],
//     degree: [
//         ObjectId('5cbdeec8952db9f5ae68950c'),
//     ],
//     industry: [
//         ObjectId('5cbdefda952db9f5ae6895ce'),
//     ],
//     occupation: null,
//     title: null,
//     company: null,
//     isActive: true,
//     email: 'hhh@mailinator.com',
//     password: '$2b$10$qxz69jqRybUgFHE2RfA56edfSFvrKF1RVMBxHml/c/P44V2PFa4ta',
//     __v: 0,
//     level: 4,
// },
// ];

// exports.skills = [
//     {
//         _id: ObjectId('5cbde37e952db9f5ae6890d7'),
//         name: 'nodejs',
//     },

//     {
//         _id: ObjectId('5cbde407952db9f5ae689117'),
//         name: 'react',
//     },

//     {
//         _id: ObjectId('5cbde40f952db9f5ae68911b'),
//         name: 'php',
//     },

//     {
//         _id: ObjectId('5cbde417952db9f5ae68911f'),
//         name: 'java',
//     }];

// exports.languages = [
//     {
//         _id: ObjectId('5cbde9ed952db9f5ae6892a5'),
//         name: 'english',
//     },

//     {
//         _id: ObjectId('5cbde9f5952db9f5ae6892b2'),
//         name: 'gujarati',
//     },

//     {
//         _id: ObjectId('5cbde9fc952db9f5ae6892b8'),
//         name: 'hindi',
//     },

//     {
//         _id: ObjectId('5cbdea09952db9f5ae6892c2'),
//         name: 'tamil',
//     },

// ];

// exports.degrees = [
//     {
//         _id: ObjectId('5cbdeeb4952db9f5ae6894eb'),
//         name: 'bcom',
//     },

//     {
//         _id: ObjectId('5cbdeebc952db9f5ae6894fd'),
//         name: 'mcom',
//     },

//     {
//         _id: ObjectId('5cbdeec8952db9f5ae68950c'),
//         name: 'bsc',
//     },

//     {
//         _id: ObjectId('5cbdeecf952db9f5ae689513'),
//         name: 'msc',
//     },

// ];

// exports.industries = [
//     {
//         _id: ObjectId('5cbdefce952db9f5ae6895bf'),
//         name: 'it',
//     },

//     {
//         _id: ObjectId('5cbdefda952db9f5ae6895ce'),
//         name: 'teaching',
//     },

//     {
//         _id: ObjectId('5cbdefe5952db9f5ae6895da'),
//         name: 'marketing',
//     },
// ];


exports.schools = [
    {
        // _id: ObjectId('5cbdeffe952db9f5ae6895bf'),
        name: 'DPS',
    },

    {
        // _id: ObjectId('5cbdefna952db9f5ae6895ce'),
        name: 'Maharana Pratap',
    },

    {
        // _id: ObjectId('5cbdefe5952db9f3ae6835da'),
        name: 'Shivaji',
    },
];


exports.curriculums = [
    {
        // _id: ObjectId('5cbdeffe952db9f5ae6895zz'),
        name: 'Chap 1',
    },

    {
        // _id: ObjectId('5cbdefna952db9f5ae6895yy'),
        name: 'Chap 2',
    },

    {
        // _id: ObjectId('5cbdefe5952db9f3ae6835mm'),
        name: 'Chap 3',
    },
];
