const testData = {
    userId: '4c31c81d2895ab56f70aaaf0',
    session_id: '3c41c91d6635ab12f70awef7',
    request_id: '537eed02ed345b2e039652d2',
    category_id: '8c58c48d0411ab56f704l6f0',
    inValidEmail: 'sagar',
    email: 'sagar@gmail.com',
    password: 'qwerty',
    authToken: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.' +
        'eyJ1c2VyX2lkIjoiNWMzNjFmNjIxNGU2Y2QzOTMyZjY4ZGU4IiwiZW1ha' +
        'WwiOiJzYWdhci5qZXRoaTAwN0BnbWFpbC5jb20iLCJ0eXBlIjoxLCJzZXNz' +
        'aW9uIjoiNjYzZmUxM2QyOTFRODhCbGMyZTZkZk01NmMzaTQiLCJpYXQiOjE1ND' +
        'cwNTIwMDMsImV4cCI6MTU0NzA3MzYwM30.8QqL3bHMnLn7L0P0r2ylTHA4pJcNVIT69VJIpEGbI_c',

    BASE_URL: 'http://localhost',
    uri: {
        login: '/api/v1/user/login',
        logout: '/api/v1/user/logout',
        userProfile: '/api/v1/userprofile',
        postQuestion: '/api/v1/question',
        getQuestionForTutor: '/api/v1/question/tutor',
        acceptRejectQuestioneUri: '/api/v1/answer/acceptReject',
    },
};

module.exports = testData;
