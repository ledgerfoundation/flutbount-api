

module.exports = {
    SERVER_ERROR_MESSAGE: 'Internal Server Error.',
    SERVICE_UNAVAILABLE: 'Service is unavailable. Please try after some time!',
    ACCESS_DENIED: 'Access Denied!',
    INVALID_REQUEST: 'Your request is invalid!!',
    INVALID_TOKEN: 'Your token is invalid',
    INVALID_CREDENTIALS: 'Invalid credentials!',
    TOKEN_EXPIRED: 'Token is expired!',
    USER_ALREADY_VERIFIED: 'User already verified!',
    USER_ALREADY_EXIST: 'User already exist!',
    USER_CREATED: 'User created!',
    USER_STATUS_CHANGE: 'Status has been changed successfully!',
    ACCESS_FORBIDDEN: 'You don\'t have sufficient access',
    STATUS_EXISTS: 'Status already exists!',
    STATUS_UPDATE: 'Status updated successfully',
    ERROR_VERIFY_EMAIL: 'Error during verify email!',
    EMAIL_NOT_REG: 'This email is not registered, please Sign Up first!',
    ALREADY_REG_WITH_EMAIL: 'You are already registered with email, Please continue with email login!',
    ERROR_VERIFY_SUCCESS: 'Email verified!',
    LOGIN_SUCCESS: 'LoggedIn successfully!',
    LOGIN_FAIL: 'Can not find user details with this credentials!',
    LOGIN_ERROR: 'Error during login!',
    LOGOUT_SUCCESS: 'LoggedOut successfully!',
    LOGOUT_ERROR: 'Error during logout!',
    QUESTION_POST_ERROR: 'Error during question post!',
    BOOKMARK_ERROR: 'Error during question bookmark!',
    RATING_ERROR: 'Error during question rating!',
    QUESTION_POST: 'Question post suessfully',
    QUESTION_DETAILS: 'Question get suessfully',
    ACCESS_TOKEN_REQUIRED: 'Access token is required!',
    UNAUTHORIZED_ACCESS: 'You don\'t have sufficient access..',
    STUDENT_NOT_FOUND: 'Student not found!',
    USER_PROFILE: 'User data get successfully ',
    USER_PROFILE_NOT_FOUND: 'User not found',
    REQUEST_ACCEPTED: 'Request has been accepted!',
    REQUEST_REJECTED: 'Request has been rejected!',
    REQUEST_ALREADY_ACCAPTED: 'Request already been accepted!',
    QUESTION_ACCEPTED_PUSH_TITLE: 'Question Accepted',
    QUESTION_ACCEPTED_PUSH_BODY: 'Your question has been accepted by tutor!',
    QUESTION_ADDED_PUSH_TITLE: 'New Question Request',
    QUESTION_ADDED_PUSH_BODY: 'New question Request in your digest!',
    SESSION_NOT_STARTED_YET: 'Session not started yet!',
    SESSION_ALREADY_COMPLETED: 'Session already completed!',
    SESSION_COMPLETED_SUCCESSFULLY: 'Session has been completed!',
    SESSION_JOINED_SUCCESSFULLY: 'Session has been joined successfully!',
    ANSWER_SUBMITTED: 'Answer has been submitted successfully!',
    USER_UPDATED: 'user has be updated successfully!',
    SUCCESS: 'Sucess !!',
    IMAGE_FILE_NOT_FOUND: 'Only image file upload',
    UPLOADED_FILES: 'Images are upload',
    NO_DATA_FOUND: 'NO DATA FOUND',
    BOOKMARK_EXISTS: 'Bookmark already exists!',

    /**
     * It will return module signup message
     *
     * @param module
     *
     * @return {string}
     */
    registeredSuccess: module => `${module} registered successfully`,

    /**
     * It will return required field message
     *
     * @param {string} fieldName
     *
     * @return {string}
     */
    checkIfRequired: fieldName => `${fieldName} is required.`,

    /**
     * It will return message for valid URL.
     *
     * @param {string} fieldName
     *
     * @return {string}
     */
    checkIfURL: fieldName => `The ${fieldName} must be a valid URL.`,

    /**
     * It will return message for alphanumeric field validation.
     *
     * @param {string} fieldName
     *
     * @return {string}
     */
    checkIfAlphaNumeric: fieldName => `The ${fieldName} must be alpha numeric.`,

    /**
     * It will return message for numeric field validation.
     *
     * @param {string} fieldName
     *
     * @return {string}
     */
    checkIfNumeric: fieldName => `The ${fieldName} field must be numeric.`,

    /**
     * It will return message for image extension validation
     *
     * @param {string} fieldName
     *
     * @return {string}
     */
    checkImageExtension: (fieldName, extension = 'jpg, png, jpeg') => `The ${fieldName} allows only (${extension}).`,

    /**
     * It will return message for valid date validation.
     *
     * @param {string} fieldName
     *
     * @return {string}
     */
    checkIfValidDate: (fieldName, dateFormat = 'YYYY-MM-DD') => `The ${fieldName} must be in a (${dateFormat})`,

    /**
     * It will return message for valid email.
     *
     * @param {string} fieldName
     *
     * @return {string}
     */
    checkIfEmail: fieldName => `${fieldName} must be a valid email address.`,

    /**
     * It will return message for valid integer.
     *
     * @param {string} fieldName
     *
     * @return {string}
     */
    checkInt: fieldName => `The ${fieldName} must be a valid integer.`,

    /**
     * It will return message for valid float value.
     *
     * @param {string} fieldName
     *
     * @return {string}
     */
    checkFloat: fieldName => `The ${fieldName} must be a valid float value.`,

    /**
     * It will return message for integer value between min and max value.
     *
     * @param {string} fieldName
     * @param {integer} min
     * @param {integer} max
     *
     * @return {string}
     */
    checkIntAndMinMax: (fieldName, min, max) => `The ${fieldName} must be an integer and` +
        ` between ${min} and ${max}.`,

    /**
     * It will return message for number value between min and max value.
     *
     * @param {string} fieldName
     * @param {integer} min
     * @param {integer} max
     *
     * @return {string}
     */
    checkNumberAndMinMax: (fieldName, min, max) => `The ${fieldName} must be a number and` +
        ` between ${min} and ${max}.`,

    /**
     * It will return message for field must be a string with min and max
     * number of characters
     *
     * @param fieldName
     * @param min
     * @param max
     *
     * @return {string}
     */
    checkStringMinAndMax: (fieldName, min, max) => `${fieldName} must be a string and` +
        ` between ${min} to ${max} characters.`,

    /**
     * It will return message for enum validation
     *
     * @param {string} fieldName
     * @param {array} enumArray
     *
     * @return {string}
     */
    checkIfValidEnum: (fieldName, enumArray) => `The ${fieldName} must have a valid value from ${enumArray}`,

    /**
     * It will return message for valid value.
     *
     * @param {string} fieldName
     *
     * @return {string}
     */
    checkIfValidValue: fieldName => `The ${fieldName} must have a valid value`,

    /**
     * It will return message for valid non empty array.
     *
     * @param {string} fieldName
     *
     * @return {string}
     */
    checkIfArray: fieldName => `The ${fieldName} must not be empty` +
        ' and it should be a valid array.',

    /**
     *  It will return message when both field are not same
     *
     * @param {string} fieldName1
     * @param {string} fieldName2
     *
     * @return {string}
     */
    checkIfSame: (fieldName1, fieldName2) => `${fieldName1} and ${fieldName2} does not match`,

    /**
     * It will return message for success in list api.
     *
     * @param {string} module - component name
     *
     * @return {string}
     */
    MODULE_LIST_SUCCESS: module => `${module} list.`,

    /**
     * It will return message for error in list api.
     *
     * @param {string} module - component name
     *
     * @return {string}
     */
    MODULE_LIST_ERROR: module => `Error while listing ${module}.`,

    /**
     * It will return message for success in store api.
     *
     * @param {string} module - component name
     *
     * @return {string}
     */
    MODULE_STORE_SUCCESS: module => `${module} added successfully!`,

    /**
     * It will return message for error is store api
     *
     * @param {string} module - component name
     *
     * @return {string}
     */
    MODULE_STORE_ERROR: module => `Error while inserting ${module} details.`,

    /**
     * It will return message for success in specific get component
     *
     * @param {string} module - component name
     *
     * @return {string}
     */
    MODULE_SHOW_SUCCESS: module => `${module} details.`,

    /**
     * It will return message for error in specific get component
     *
     * @param {string} module - component name
     *
     * @return {string}
     */
    MODULE_SHOW_ERROR: module => `Error during getting ${module} details.`,

    /**
     * It will return message for components is already exist
     *
     * @param {string} module - component name
     *
     * @return {string}
     */
    MODULE_EXISTS: module => `The ${module} already exists.`,

    /**
     * It will return message while component found
     *
     * @param {string} module - component name
     *
     * @return {string}
     */
    MODULE_FOUND: module => `${module} found.`,

    /**
     * It will return message while component not found error
     *
     * @param {string} module - component name
     *
     * @return {string}
     */
    MODULE_NOT_FOUND: module => `The ${module} you are looking for is not found.`,

    /**
     * It will return message for success in update component.
     *
     * @param {string} module - component name
     *
     * @return {string}
     */
    MODULE_UPDATE_SUCCESS: module => `${module} details has been updated successfully!`,

    /**
     * It will return message for error in update component
     *
     * @param {string} module - component name
     *
     * @return {string}
     */
    MODULE_UPDATE_ERROR: module => `Could not able update ${module} details.`,

    /**
     * It will return message for components if already updated
     *
     * @param {string} module - component name
     * @param {string} status - status name
     *
     * @return {string}
     */
    MODULE_STATUS_EXISTS: (module, status) => `${module} has already ${status}!`,

    /**
     * It will return message for success in enable/disable component.
     *
     * @param {string} module - component name
     * @param {string} status - enable / disable
     *
     * @return {string}
     */
    MODULE_STATUS_CHANGE: (module, status) => `${module} ${status} successfully!`,

    /**
     * It will return message for error in enable/disable component.
     *
     * @param {string} module - component name
     * @param {string} status - enable / disable
     *
     * @return {string}
     */
    MODULE_STAUS_CHANGE_ERROR: (module, status) => `Error while ${status} ${module}.`,

    /**
     * It will return message for error in delete component.
     *
     * @param {string} module - component name
     *
     * @return {string}
     */
    MODULE_DELETE_ERROR: module => `Error while deleting ${module}`,

    /**
     * It will return message for success in delete component.
     *
     * @param {string} module - component name
     *
     * @return {string}
     */
    MODULE_DELETE_SUCCESS: module => `The ${module} has been deleted successfully`,

    /**
     * It will return message for error in uploaded file size limit
     *
     * @param {string} field - image field name
     * @param {number} size  - size of validated file.
     *
     * @return {string}
     */
    FILE_SIZE_LIMIT: (field, size) => `The ${field} size should be less than ${size} MB.`,

    /**
     * It will return message for success during file upload.
     *
     * @param {string} field - image field name
     *
     * @return {string}
     */
    FILE_UPLOAD_SUCCESS: field => `The ${field} has been uploaded successfully.`,

    /**
     * It will return message for valid non empty string.
     *
     * @param {string} fieldName
     * @param {integer} min
     * @param {integer} max
     *
     * @return {string}
     */
    checkLength: (fieldName, min, max) => {
        const minimum = typeof min !== 'undefined' ? min.toString() : '';
        const maximum = typeof max !== 'undefined' ? max.toString() : '';

        if (minimum === maximum) {
            return `${fieldName} length should be ${maximum} characters.`;
        } else if (maximum === '') {
            return `${fieldName} length must be at least ${minimum} characters.`;
        } else if (minimum === '') {
            return `${fieldName} length should not be greater` +
                ` than ${maximum} characters.`;
        }
        return `${fieldName} length must be between` +
            ` ${minimum} to ${maximum} characters.`;
    },
    /**
     * It will return message for valid json.
     *
     * @param {String} fieldName
     *
     * @return {String}
     */
    checkIfValidJSON: fieldName => `The ${fieldName} field must be a valid JSON String.`,

    // file upload
    FILE_TYPE_MISMATCH(module) {
        return `The ${module} must be a valid image file.`;
    },
    FILE_UPLOAD_ERROR: 'Error during file upload.',
};
