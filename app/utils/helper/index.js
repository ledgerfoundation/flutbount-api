/* eslint-disable no-undef */
'use strict';

module.exports = {
    /**
     * It will return random value between min and max value.
     *
     * @return {number}
     */
    getRandomInt: (min, max) => {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },

    /**
     * It will return random alphanumeric string of a given length
     *
     * @param {integer} length
     *
     * @return {string}
     */
    randomStr: (length) => {
        const possible = '0123456789abcdefghijklmnopqrstuvw' +
            'xyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        let randomText = '';
        let i;

        for (i = 0; i < length; i++) {
            randomText += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        return randomText;
    },

    /**
     * It will check if passed value is an array or not
     *
     * @param {array} value
     *
     * @return {boolean}
     */
    isArray: (value) => {
        return Array.isArray(value);
    },

    shuffleString: (value) => {
        return value.split('')
            .sort(() => { return 0.5 - Math.random(); })
            .join('');
    },
};
