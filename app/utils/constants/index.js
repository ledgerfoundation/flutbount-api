'use strict';

module.exports = {
    // userType
    userType: {
        student: 1,
        tutor: 2,
        parent: 3,
        admin: 4,
    },

    // deviceType
    deviceType: {
        android: 1,
        iOS: 2,
        web: 3,
        other: 4,
    },
    //XP
    XP : {
        xp: 10,
    },
    // questionRequest
    questionRequestType: {
        pending: 0,
        accept: 1,
        reject: 2,
        expire: 3,
    },

    // sessionStatus
    sessionStatus: {
        not_started: 0,
        started: 1,
        completed: 2,
    },
// User Session Status
     userSessionStatus: {
        activeAsk: 0,
        activeAnswer: 1,
        completedAsk: 2,
        completedAnswer: 3,
       
    },

    UPLOAD_IMAGE_EXTENSIONS: ['jpeg', 'jpg', 'png'],
    UPLOAD_IMAGE_TYPE: ['image/jpeg', 'image/jpg', 'image/png'],
    MAX_IMAGE_SIZE: 5, // this will be in MBs
};
