/* eslint-disable no-undef */
'use strict';

const FCM = require('fcm-push');
const config = require('../../../config');

const serverKey = config.serverConfig.getFcmServerKey();

const fcmObj = new FCM(serverKey);

module.exports = {
    sendPushNotification: async (deviceTokenArr, title, body,data={}) => {
        return new Promise((resolve, reject) => {
            try {
                const message = {
                    registration_ids: deviceTokenArr,
                    notification: {
                        title,
                        body,
                    },
                    datat:data
                };
                const sendNotification = fcmObj.send(message);
                resolve(sendNotification);
            } catch (err) {
                logger.error(`Error in send push: ${err}`);
                resolve(err);
            }
        });
    },
};
