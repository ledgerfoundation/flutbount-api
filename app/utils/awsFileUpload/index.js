'use strict';

const AWS = require('aws-sdk');
const config = require('../../../config');
const logger = require('../../utils/logging');

let s3Bucket = new AWS.S3({
    accessKeyId: config.aws.getAccessKey(),
    secretAccessKey: config.aws.getSecretKey(),
    Bucket: config.aws.getBucketName(),
    region: config.aws.getRegion()
});

module.exports = {
    uploadFile: async (fileName, bufferData, filePath, isPublic = true) => {
        return new Promise((resolve, reject) => {
            try {
                let uploadParams = {
                    Bucket: filePath,
                    Key: fileName,
                    Body: bufferData,
                    ACL: 'public-read',
                };

                if (isPublic) {
                    s3Bucket.createBucket(() => {
                        s3Bucket.putObject(uploadParams, (err, data) => {
                            if (err) {
                                logger.error('Error during public file upload in aws : ', err);
                                reject('false');
                            } else {
                                resolve('true');
                            }
                        });
                    });
                } else {
                    s3Bucket.createBucket(() => {
                        s3Bucket.putObject(uploadParams, (err, data) => {
                            if (err) {
                                logger.errors('Error during private file upload in aws : ', err);
                                reject('false');
                            } else {
                                resolve('true');
                            }
                        });
                    });
                }
            } catch (err) {
                logger.error('Error during upload in file in aws s3', err);
                resolve('false');
            }
        });
    },

    getPreSignedUrl: async (fileName) => {
        return new Promise((resolve, reject) => {
            try {
                let signUrl = s3Bucket.getSignedUrl('getObject', {
                    Bucket: config.aws.getBucketName(),
                    Key: fileName.substr(1), //remove '/' from starting
                    Expires: 300 //5min * 60sec
                });
                resolve(signUrl);
            } catch (err) {
                reject(err);
            }
        });
    },

    deleteFile: async (fileName, is_public) => {
        return new Promise((resolve, reject) => {
            try {
                let params = {
                    Bucket: config.aws.getBucketName(),
                    Key: fileName,
                };

                s3Bucket.deleteObject(params, function (err, data) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
            } catch (err) {
                logger.error('Error during delete the file from AWS S3 bucket!', err)
                reject(err);
            }
        });
    }
};
