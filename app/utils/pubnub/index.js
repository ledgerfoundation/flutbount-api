'use strict';

const PubNub = require('pubnub');
const config = require('../../../config');
const logger = require('../../utils/logging');

//Configuration



var pubnub = new PubNub({
    subscribeKey: config.pubnub.getSubscribeKey(),
    publishKey: config.pubnub.getPublishKey(),
    secretKey: config.pubnub.getSecretKey(),
    ssl: false
})

//publish Message
module.exports = {

       publishMessage : async (channelName='',message={},sendByPost=true,storeInHistory=true,meta={},msgPublishTime='') =>{
        console.log("Since we're publishing on subscribe connectEvent, we're sure we'll receive the following publish.");
        var publishConfig = {
            channel : channelName,
            message : message,
            sendByPost: sendByPost, // true or false
            storeInHistory: storeInHistory, //true or false
            meta: meta,
            ttl: msgPublishTime
        }
        pubnub.publish(publishConfig, function(status, response) {
            
            console.log(message);
            if (status.error) {
                // handle error
                console.log(status)
            } else {
                console.log("message Published w/ timetoken", response.timetoken)
                
            }
          return status;
        })
    },

    //Subscribe

    subscribe : async (channelName=[]) =>{
        console.log(channelName);
                var SubscribeConfig = {
                    channels : channelName,
                }
                pubnub.subscribe(SubscribeConfig, function(status, response) {
                    console.log("hii");
                    console.log(status, response);  
                })
    },


    //unSubscribe

    unSubscribe : async (channelName=[],channelGroups=[]) =>{
                var unSubscribeConfig = {
                    channels : channelName,
                    channelGroups : channelGroups
                
                }
                pubnub.unsubscribe(unSubscribeConfig, function(status, response) {
                    console.log(status, response);
                })
    },
    //unsubscribeAll

     unSubscribeAll : async() =>{
                pubnub.unsubscribeAll(function(status, response) {
                    
                    console.log(status, response);
                })
            },
    //Listner

     listner : async (msg={},pres={},sta={}) => {
                pubnub.addListener({
                    message: function() {
                        // handle message
                        var channelName = msg.channel; // The channel for which the message belongs
                        var channelGroup = msg.subscription; // The channel group or wildcard subscription match (if exists)
                        var pubTT = msg.timetoken; // Publish timetoken
                        var msg = msg.message; // The Payload
                        var publisher = msg.publisher; //The Publisher
                    },
                    presence: function() {
                        // handle presence
                        var action = pres.action; // Can be join, leave, state-change or timeout
                        var channelName = pres.channel; // The channel for which the message belongs
                        var occupancy = pres.occupancy; // No. of users connected with the channel
                        var state = pres.state; // User State
                        var channelGroup = pres.subscription; //  The channel group or wildcard subscription match (if exists)
                        var publishTime = pres.timestamp; // Publish timetoken
                        var timetoken = pres.timetoken;  // Current timetoken
                        var uuid = pres.uuid; // UUIDs of users who are connected with the channel
                    },
                    status: function() {
                        var affectedChannelGroups = sta.affectedChannelGroups;
                        var affectedChannels = sta.affectedChannels;
                        var category = sta.category;
                        var operation = sta.operation;
                    }
                });
            
     },
    //addChannels




     addChannels : async (channelName=[],channelGroup='') =>{
                var addChannelsConfig = {
                    channels : channelName,
                    channelGroup : channelGroup
                
                }
                pubnub.channelGroups.addChannels(addChannelsConfig, function(status, response) {
                    console.log(status, response);
                })
            },
    //listChannels


    listChannels : async (channelName=[],channelGroup='') =>{
                var listChannelsConfig = {
                    channels : channelName,
                    channelGroup : channelGroup
                
                }
                pubnub.channelGroups.listChannels(listChannelsConfig, function(status, response) {
                    console.log(status, response);
                })
            },
    //deleteGroup

     deleteGroup : async (channelGroup='') =>{
                var deleteGroupConfig = {
                    
                    channelGroup : channelGroup
                
                }
                pubnub.channelGroups.deleteGroup(deleteGroupConfig, function(status, response) {
                    console.log(status, response);  
                })
            },


    //removeChannels



    removeChannels : async(channelName=[],channelGroup='') =>{
                var removeChannelsConfig = {
                    channels : channelName,
                    channelGroup : channelGroup
                
                }
                pubnub.channelGroups.removeChannels(removeChannelsConfig, function(status, response) {
                    console.log(status, response);  
                })
    },


    //history

 
    
    history : async (channelName='',count='',start='',end='') =>{
        return new Promise((resolve, reject) => {
            try {
                var historyConfig = {
                    channel : channelName,
                    count: count, // how many items to fetch
                    reverse : true, // Setting to true will traverse the time line in reverse starting with the oldest message first.
                    stringifiedTimeToken: true, // false is the default        
                    start: start, // start time token to fetch
                    end: end // start time token to fetch
                }
                pubnub.history(historyConfig, function(status, response) {
                
                    if (status.error===true) {
                        reject(status);
                    } else {
                        resolve(response);
                    }
                })
            }catch(error){
                reject(err);
            }

        });
    },

    //hereNow

    herenow : async (channelName=[],groupsName=[]) =>{
        return new Promise((resolve, reject) => {
            try {
                var herenowConfig = {
                    channel : channelName,
                    channelGroups : groupsName,
                    includeUUIDs: true,
                    includeState: true
                }
                pubnub.hereNow(herenowConfig, function(status, response) {
                
                    if (status.error===true) {
                        reject(status);
                    } else {
                        resolve(response);
                    }
                })
            }catch(error){
                reject(err);
            }

        });
    },


}


// channels: ["ch1"], 
// channelGroups : ["cg1"],
// includeUUIDs: true,
// includeState: true