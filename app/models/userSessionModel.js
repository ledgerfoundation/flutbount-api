/**
 * user session model
 */
const mongoose = require('mongoose');

const userSessionSchema = mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        required: true,
    },
    session_id: {
        type: String,
        required: true,
    },
    session_timeout: {
        type: Number,
        default: null, // Session time out value in sesconds
    },
    stay_logged_in: {
        type: Boolean,
        default: false,
    },
    login_date: {
        type: Date,
        default: new Date().getTime(),
    },
    logout_date: {
        type: Date,
        default: null,
    },
    isActive: {
        type: Boolean,
        default: true,
    },
    last_access_date: {
        type: Date,
        default: new Date().getTime(),
    },
    device_type: {
        type: Number,
        require: true,
        enum: [1, 2, 3], // [1-"android", 2-"iOS",3-"web", 4-"Other"]
    },
    device_token: {
        type: String,
        require: true,
    },
}, { timestamps: true });

module.exports = mongoose.model('user_sessions', userSessionSchema);
