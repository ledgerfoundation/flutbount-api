const userModel = require('./userModel');
// const studentModel = require('./studentModel');
const userMetaModel = require('./userMetaModel');
// const answerModel = require('./answerModel');
// const questionModel = require('./questionModel');
const categoryModel = require('./categoryModel');
const schoolModel = require('./schoolModel');
const CurriculumModel = require('./curriculumModel');

// const requestAnswerModel = require('./requestAnswerModel');
// const ratingModel = require('./ratingModel');
// const ratingModel = require('./reportModel');
// const ratingModel = require('./reportTypesModel');
// const matchModel = require('./matchModel');
const bookmarkModel = require('./bookmarkModel');
const xpTransactionModel = require('./xpTransactionModel');

module.exports = {
    userModel,
    userMetaModel,
    // answerModel,
    questionModel,
    categoryModel,
    requestAnswerModel,
    // ratingModel,
    bookmarkModel,
    xpTransactionModel,
    schoolModel,
    CurriculumModel,
};
