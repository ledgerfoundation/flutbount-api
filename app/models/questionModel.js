/**
 * user model
 */
const mongoose = require('mongoose');

const questionSchema = mongoose.Schema({
    title: {
        type: String,
        trim: true,
        required: true,
    },
    description: {
        type: String,
    },
    category: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'catergory',
        required: true,
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
    },
    
    expert: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
    },
    sessionStartDate: {
        type: Date,
        default: null,
    },
    sessionEndDate: {
        type: Date,
        default: null,
    },
    expertSessionStatus: {
        type: Number,
        default: null,
    },
    userSessionStatus: {
        type: Number,
        default: null,
    },
    sessionStatus: {
        type: Number,
        default: 0,
    },
    isActive: {
        type: Boolean,
        default: true,
    },
    questionImage: {
        type: String,
        default: null,
    },
}, { timestamps: true });

module.exports = mongoose.model('question', questionSchema);
