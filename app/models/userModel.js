/**
 * user model
 */
const mongoose = require('mongoose');
const crypto = require('crypto');

const userSchema = mongoose.Schema({
    email: {
        type: String,
        lowercase: true,
        trim: true,
        index: true,
        unique: true,
        required: true,
    },
    hash: {
        type: String,
    },
    salt: {
        type: String,

    },

    isActive: {
        type: Boolean,
        default: false,
    },
    authToken: {
        type: String,
    },
    online_status :{
        type: Number,
      
    },
    last_access_date:{
        type: Date,
    },
    google: {
        id: String,
        token: String,
    },
}, {
    timestamps: true,
});

module.exports = mongoose.model('user', userSchema);
