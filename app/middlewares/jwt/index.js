const jwt = require('jsonwebtoken');
const response = require('../../utils/ResponseService');
const messages = require('../../utils/messages');
const config = require('../../../config');
const logger = require('../../utils/logging');
const userModel = require('../../../app/components/user/user.model');
const constants = require('../../utils/constants');
const { check, validationResult } = require('express-validator/check');
const { matchedData, sanitize } = require('express-validator/filter');

exports.verifyJWT = async function (req, res, next) {
    const token = req.headers.authorization; // Parse token from header

    if (!token) {
        response.send(res, {
            status: response.getCode().codes.BAD_REQUEST,
            message: messages.ACCESS_TOKEN_REQUIRED,
            data: {},
        });
        return;
    }

    // verify the jwt token
    try {
        const decoded = await module.exports.verifyToken(token);
        console.log(decoded);
        if (typeof decoded.userId === 'undefined'
            || typeof decoded.session === 'undefined'
        ) {
            response.send(res, {
                status: response.getCode().codes.UNAUTHORIZED,
                message: 'messages.UNAUTHORIZED_ACCESS',
                data: {},
            });
            return;
        }

        // check user session is alive or not
        const userSession = await userModel.findUserSession({
            userId: decoded.userId,
            session_id: decoded.session,
        });

        if (userSession.length <= 0) {
            response.send(res, {
                status: response.getCode().codes.UNAUTHORIZED,
                message: messages.UNAUTHORIZED_ACCESS,
                data: {},
            });
            return;
        }

        // add loggedIn user id in request
        req.userId = decoded.userId;


        next();
    } catch (err) {
        logger.error(`Error in JWT Middleware: ${err.message}`);
        response.send(res, {
            status: response.getCode().codes.INTERNAL_SERVER_ERROR,
            message: messages.INVALID_TOKEN,
            data: {},
        });
    }
};

exports.generateJWT = async (userId, userEmail, sessionId) => {
    const token = await jwt.sign({
        userId: userId,
        email: userEmail,
        session: sessionId,
    }, config.serverConfig.getTokenSecret(), {
        expiresIn: config.serverConfig.getTokenExpirationTime(),
    });
    return token;
};

exports.verifyToken = async token => new Promise(async (resolve, reject) => {
    try {
        // split token from bearer keyword
        const tokenData = typeof token.split(' ')[1] === 'undefined' ? token : token.split(' ')[1];

        const decoded = await jwt.verify(tokenData, config.serverConfig.getTokenSecret());
        resolve(decoded);
    } catch (err) {
        logger.error(`Error during verifyJWT: ${err.message}`);
        reject(err);
    }
});

// This will be used to sent verification mail
exports.generateEmailjwt = async (email) => {
    const token = await jwt.sign({
        email,
    }, config.serverConfig.getTokenSecret(), {
        expiresIn: config.serverConfig.getTokenExpirationTime(),
    });
    return token;
};

