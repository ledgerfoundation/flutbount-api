const bodyParser = require('body-parser');
const cors = require('cors');
const fileUpload = require('express-fileupload');

class CommonMiddlewares {
    constructor(app) {
        this.app = app;
        this.initialize();
    }

    initialize() {
        this.app.use(cors());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(bodyParser.json());
        this.app.use(fileUpload());
    }
}


module.exports = CommonMiddlewares;
