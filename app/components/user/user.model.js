/* eslint-disable no-underscore-dangle */


const User = require('../../models/userModel');
const UserMeta = require('../../models/userMetaModel');
const UserSession = require('../../models/userSessionModel');
const logger = require('../../utils/logging');

module.exports = {
    findUser: async (whereArr, fields = {}) => new Promise(async (resolve, reject) => {
        try {
            const userDetails = await User
                .findOne(whereArr, fields);

            logger.info('user details found!');

            resolve(userDetails);
        } catch (err) {
            logger.error(`error during find userDetails ${err}`);
            reject(err);
        }
    }),
    createUser: async (userData = {}) => new Promise(async (resolve, reject) => {
        try {
            // creating empty user object
            const createUser = new User();

            // intialize newUser object with request data
            createUser.email = userData.email;
            // call setPassword function to hash password
            // createUser.__proto__.setPassword(userData.password);

            await createUser.save();
            resolve(createUser);
        } catch (err) {
            logger.error(`Error during create user: ${err}`);
            reject(err);
        }
    }),

    updateUser: async (whereClaus, updateData) => new Promise(async (resolve, reject) => {
        try {
            const updateUser = User.findOneAndUpdate(whereClaus, updateData,{upsert:true});
            logger.info('User details updated successfully!');
            resolve(updateUser);
        } catch (err) {
            logger.error(`Error during update user details: ${err}`);
            reject(err);
        }
    }),
     updateUserMeta: async (whereArr, updateData) => new Promise(async (resolve, reject) => {
        try {
            const updateUserMeta = UserMeta.findOneAndUpdate(whereArr, updateData,{upsert:true});
            logger.info('User Meta updated successfully!');
            resolve(updateUserMeta);
        } catch (err) {
            logger.error(`error during find userSession ${err}`);
            reject(err);
        }
    }),

    crerateUserSession: async (userSessionData = {}) => new Promise(async (resolve, reject) => {
        try {
            const userSession = new UserSession(userSessionData);
            await userSession.save();

            resolve(userSession);
        } catch (err) {
            logger.error(`Error during store user session: ${err}`);
            reject(err);
        }
    }),

    deleteUserSession: async whereClaus => new Promise(async (resolve, reject) => {
        try {
            const userSession = await UserSession
                .deleteOne(whereClaus);
            resolve(userSession);
        } catch (err) {
            logger.error(`Error during delete user session: ${err}`);
            reject(err);
        }
    }),

    findUserSession: async (whereArr, fields = {}) => new Promise(async (resolve, reject) => {
        try {
            const userSession = await UserSession
                .find(whereArr, fields);
            logger.info('user session found!');
            resolve(userSession);
        } catch (err) {
            logger.error(`error during find userSession ${err}`);
            reject(err);
        }
    }),
};
