const chai = require('chai');
const expect = chai.expect;
const constants = require('../../utils/constants');
const responseService = require('../../utils/ResponseService');
const testData = require('../../../config/testing/testData');
const testHelper = require('../../../tests/helperTest');
const messages = require('../../utils/messages');
const userModel = require('./user.model');
const tutorModel = require('../tutor/tutor.model');
const jwtMiddleware = require('../../middlewares/jwt');

// Test cases for Institute modules
describe('Test cases for User modules', () => {

    beforeEach(() => {});

    afterEach(() => {});

    before(() => {});

    after(() => {});

    describe('Login API', () => {
        let loginObject = {};
        beforeEach(() => {
            loginObject = {
                firstName: 'firstName',
                lastName: 'lastName',
                profile_image: 'profile_image',
                email: 'email@example.com',
                googleId: 'googleId',
                userType: constants.userType['student'],
                deviceType: constants.deviceType['android'],
                deviceToken: 'deviceToken'
            };
        });

        afterEach( () => {
            loginObject = {}
        });
        // send empty body in request
        testHelper.checkEmptyBody(testData.uri.login, 'post', {}, 'form');

        // send empty firstName in request
        testHelper.checkEmptyKey(testData.uri.login, 'post', loginObject , 'form', '', '', 'firstName');

        // send empty lastName in request
        testHelper.checkEmptyKey(testData.uri.login, 'post', loginObject , 'form', '', '', 'lastName');

        // send empty profile_image in request
        testHelper.checkEmptyKey(testData.uri.login, 'post', loginObject , 'form', '', '', 'profile_image');

        // send empty email in request
        testHelper.checkEmptyKey(testData.uri.login, 'post', loginObject , 'form', '', '', 'email');

        // send empty googleId in request
        testHelper.checkEmptyKey(testData.uri.login, 'post', loginObject , 'form', '', '', 'googleId');

        // send empty userType in request
        testHelper.checkEmptyKey(testData.uri.login, 'post', loginObject , 'form', '', '', 'userType');

        // send empty deviceType in request
        testHelper.checkEmptyKey(testData.uri.login, 'post', loginObject , 'form', '', '', 'deviceType');

        // send empty deviceToken in request
        testHelper.checkEmptyKey(testData.uri.login, 'post', loginObject , 'form', '', '', 'deviceToken');

        // send an invalid email
        it('send an invalid email in request should have status code 400', async () => {
            try {
                let loginObj = testHelper.generateObject(loginObject, '', [ {
                    key: 'email',
                    value: 'email'
                } ]);
                let res = await testHelper.chaiPostRequest(testData.uri.login, loginObj, 'form');

                testHelper.checkValidOutput(res);
                expect(res).to.have.status(responseService.getCode().codes.BAD_REQUEST[0]);
                expect(res.body.message).to.eql(messages.checkIfEmail('Email'))
            } catch (err) {
                testHelper.checkValidOutput(err);
                expect(err).to.have.status(responseService.getCode().codes.BAD_REQUEST[0]);
            }
        });

        // send an invalid userType
        it('send an invalid userType in request should have status code 400', async () => {
            try {
                let loginObj = testHelper.generateObject(loginObject, '', [ {
                    key: 'userType',
                    value: constants.userType['parent']
                } ]);
                let res = await testHelper.chaiPostRequest(testData.uri.login, loginObj, 'form');

                testHelper.checkValidOutput(res);
                expect(res).to.have.status(responseService.getCode().codes.BAD_REQUEST[0]);
                expect(res.body.message).to.eql(messages.checkIfValidValue('User type field'))
            } catch (err) {
                testHelper.checkValidOutput(err);
                expect(err).to.have.status(responseService.getCode().codes.BAD_REQUEST[0]);
            }
        });

        // send an invalid deviceType
        it('send an invalid deviceType in request should have status code 400', async () => {
            try {
                let loginObj = testHelper.generateObject(loginObject, '', [ {
                    key: 'deviceType',
                    value: 5
                } ]);
                let res = await testHelper.chaiPostRequest(testData.uri.login, loginObj, 'form');

                testHelper.checkValidOutput(res);
                expect(res).to.have.status(responseService.getCode().codes.BAD_REQUEST[0]);
                expect(res.body.message).to.eql(messages.checkIfValidValue('Device type'))
            } catch (err) {
                testHelper.checkValidOutput(err);
                expect(err).to.have.status(responseService.getCode().codes.BAD_REQUEST[0]);
            }
        });

        // Generate Error on userModel.findUser
        it('Generate error on userModel.findUser should have status code 500', async () => {
            try {
                // call stub function of userModel
                const userModelStub = testHelper.createStub(userModel, 'findUser');
                userModelStub.onCall(0).callsFake(() => Promise.reject('Error message goes here!'));

                let res = await testHelper.chaiPostRequest(testData.uri.login, loginObject, 'form');

                testHelper.checkValidOutput(res);
                expect(res).to.have.status(responseService.getCode().codes.INTERNAL_SERVER_ERROR[0]);
                expect(res.body.message).to.eql(messages.LOGIN_ERROR);
                userModelStub.restore();
            } catch (err) {
                testHelper.checkValidOutput(err);
                expect(err).to.have.status(responseService.getCode().codes.INTERNAL_SERVER_ERROR[0]);
            }
        });

        // Generate Error on user type mismatch
        it('Generate Error on user type mismatch should have status code 422', async () => {
            try {
                // call stub function of userModel
                const userModelStub = testHelper.createStub(userModel, 'findUser');
                userModelStub.onCall(0).callsFake(() => Promise.resolve({
                    _id: 'qwe13123',
                    email: loginObject.email,
                    user_type: 2,

                }));

                let res = await testHelper.chaiPostRequest(testData.uri.login, loginObject, 'form');
                testHelper.checkValidOutput(res);
                expect(res).to.have.status(responseService.getCode().codes.UNPROCESSED[0]);
                expect(res.body.message).to.eql(messages.INVALID_CREDENTIALS);
                userModelStub.restore();
            } catch (err) {
                testHelper.checkValidOutput(err);
                expect(err).to.have.status(responseService.getCode().codes.UNPROCESSED[0]);
            }
        });

        // Generate Error on create user session and JWT
        it('Generate Error on create user session and JWT should have status code 500', async () => {
            try {
                // call stub function of userModel
                const userModelStub = testHelper.createStub(userModel, 'findUser');
                userModelStub.onCall(0).callsFake(() => Promise.resolve({
                    _id: 'qwe13123',
                    email: loginObject.email,
                    user_type: loginObject.userType,

                }));

                // call stub function of userModel
                const crerateUserSessionStub = testHelper.createStub(userModel, 'crerateUserSession');
                crerateUserSessionStub.onCall(0).callsFake(() => Promise.reject(0));

                let res = await testHelper.chaiPostRequest(testData.uri.login, loginObject, 'form');
                testHelper.checkValidOutput(res);
                expect(res).to.have.status(responseService.getCode().codes.INTERNAL_SERVER_ERROR[0]);
                expect(res.body.message).to.eql(messages.LOGIN_ERROR);
                userModelStub.restore();
                crerateUserSessionStub.restore();
            } catch (err) {
                testHelper.checkValidOutput(err);
                expect(err).to.have.status(responseService.getCode().codes.INTERNAL_SERVER_ERROR[0]);
            }
        });

        // Send Valid Request
        it('Send Valid Request should have status code 200', async () => {
            try {
                // call stub function of userModel
                const userModelStub = testHelper.createStub(userModel, 'findUser');
                userModelStub.onCall(0).callsFake(() => Promise.resolve({
                    _id: 'qwe13123',
                    email: loginObject.email,
                    user_type: loginObject.userType,

                }));

                // call stub function of userModel
                const crerateUserSessionStub = testHelper.createStub(userModel, 'crerateUserSession');
                crerateUserSessionStub.onCall(0).callsFake(() => Promise.resolve(1));

                let res = await testHelper.chaiPostRequest(testData.uri.login, loginObject, 'form');
                testHelper.checkValidOutput(res);
                expect(res).to.have.status(responseService.getCode().codes.OK[0]);
                expect(res.body.message).to.eql(messages.LOGIN_SUCCESS);
                userModelStub.restore();
                crerateUserSessionStub.restore();
            } catch (err) {
                testHelper.checkValidOutput(err);
                expect(err).to.have.status(responseService.getCode().codes.OK[0]);
            }
        });

        // Generate Error in userModel.createUser
        it('Generate Error in userModel.createUser should have status code 500', async () => {
            try {
                // call stub function of userModel
                const userModelStub = testHelper.createStub(userModel, 'findUser');
                userModelStub.onCall(0).callsFake(() => Promise.resolve(0));

                // call stub function of userModel
                const crerateUserUserStub = testHelper.createStub(userModel, 'createUser');
                crerateUserUserStub.onCall(0).callsFake(() => Promise.reject(0));

                let res = await testHelper.chaiPostRequest(testData.uri.login, loginObject, 'form');
                testHelper.checkValidOutput(res);
                expect(res).to.have.status(responseService.getCode().codes.INTERNAL_SERVER_ERROR[0]);
                expect(res.body.message).to.eql(messages.SERVER_ERROR_MESSAGE);
                userModelStub.restore();
                crerateUserUserStub.restore();
            } catch (err) {
                testHelper.checkValidOutput(err);
                expect(err).to.have.status(responseService.getCode().codes.INTERNAL_SERVER_ERROR[0]);
            }
        });

        // Generate Error in user session and JWT
        it('Generate Error in user session and JWT should have status code 500', async () => {
            try {
                // call stub function of userModel
                const userModelStub = testHelper.createStub(userModel, 'findUser');
                userModelStub.onCall(0).callsFake(() => Promise.resolve(0));

                // call stub function of userModel
                const crerateUserStub = testHelper.createStub(userModel, 'createUser');
                crerateUserStub.onCall(0).callsFake(() => Promise.resolve({
                    _id: '5c361f6214e6cd3932f68de8',
                    email: loginObject.email,
                    user_type: loginObject.userType,
                }));

                // call stub function of userModel
                const crerateUserSessionStub = testHelper.createStub(userModel, 'crerateUserSession');
                crerateUserSessionStub.onCall(0).callsFake(() => Promise.reject(0));

                let res = await testHelper.chaiPostRequest(testData.uri.login, loginObject, 'form');
                testHelper.checkValidOutput(res);
                expect(res).to.have.status(responseService.getCode().codes.INTERNAL_SERVER_ERROR[0]);
                expect(res.body.message).to.eql(messages.SERVER_ERROR_MESSAGE);
                userModelStub.restore();
                crerateUserStub.restore();
                crerateUserSessionStub.restore();
            } catch (err) {
                testHelper.checkValidOutput(err);
                expect(err).to.have.status(responseService.getCode().codes.INTERNAL_SERVER_ERROR[0]);
            }
        });

        // Generate Error in create Student User
        it('Generate Error in create Student User should have status code 500', async () => {
            try {
                // call stub function of userModel
                const userModelStub = testHelper.createStub(userModel, 'findUser');
                userModelStub.onCall(0).callsFake(() => Promise.resolve(0));

                // call stub function of userModel
                const crerateUserStub = testHelper.createStub(userModel, 'createUser');
                crerateUserStub.onCall(0).callsFake(() => Promise.resolve({
                    _id: '5c361f6214e6cd3932f68de8',
                    email: loginObject.email,
                    user_type: loginObject.userType,
                }));

                // call stub function of userModel
                const crerateUserSessionStub = testHelper.createStub(userModel, 'crerateUserSession');
                crerateUserSessionStub.onCall(0).callsFake(() => Promise.resolve(0));

                // call stub function of userModel
                const crerateStudentStub = testHelper.createStub(studentModel, 'createStudnet');
                crerateStudentStub.onCall(0).callsFake(() => Promise.reject(0));

                let res = await testHelper.chaiPostRequest(testData.uri.login, loginObject, 'form');
                testHelper.checkValidOutput(res);
                expect(res).to.have.status(responseService.getCode().codes.INTERNAL_SERVER_ERROR[0]);
                expect(res.body.message).to.eql(messages.SERVER_ERROR_MESSAGE);
                userModelStub.restore();
                crerateUserStub.restore();
                crerateUserSessionStub.restore();
                crerateStudentStub.restore();
            } catch (err) {
                testHelper.checkValidOutput(err);
                expect(err).to.have.status(responseService.getCode().codes.INTERNAL_SERVER_ERROR[0]);
            }
        });

        // Generate Error in create Tutor User
        it('Generate Error in create Tutor User should have status code 500', async () => {
            try {
                // call stub function of userModel
                const userModelStub = testHelper.createStub(userModel, 'findUser');
                userModelStub.onCall(0).callsFake(() => Promise.resolve(0));

                // call stub function of userModel
                const crerateUserStub = testHelper.createStub(userModel, 'createUser');
                crerateUserStub.onCall(0).callsFake(() => Promise.resolve({
                    _id: '5c361f6214e6cd3932f68de8',
                    email: loginObject.email,
                    user_type: loginObject.userType,
                }));

                // call stub function of userModel
                const crerateUserSessionStub = testHelper.createStub(userModel, 'crerateUserSession');
                crerateUserSessionStub.onCall(0).callsFake(() => Promise.resolve(0));

                // call stub function of userModel
                const crerateTutorStub = testHelper.createStub(tutorModel, 'createTutor');
                crerateTutorStub.onCall(0).callsFake(() => Promise.reject(0));

                let loginObj = testHelper.generateObject(loginObject,'', [ {
                    key: 'userType',
                    value: constants.userType['tutor']
                } ])

                let res = await testHelper.chaiPostRequest(testData.uri.login, loginObj, 'form');
                testHelper.checkValidOutput(res);
                expect(res).to.have.status(responseService.getCode().codes.INTERNAL_SERVER_ERROR[0]);
                expect(res.body.message).to.eql(messages.SERVER_ERROR_MESSAGE);
                userModelStub.restore();
                crerateUserStub.restore();
                crerateUserSessionStub.restore();
                crerateTutorStub.restore();
            } catch (err) {
                testHelper.checkValidOutput(err);
                expect(err).to.have.status(responseService.getCode().codes.INTERNAL_SERVER_ERROR[0]);
            }
        });

        // Send Valid Request
        it('Send Valid Request should have status code 200', async () => {
            try {
                // call stub function of userModel
                const userModelStub = testHelper.createStub(userModel, 'findUser');
                userModelStub.onCall(0).callsFake(() => Promise.resolve(0));

                // call stub function of userModel
                const crerateUserStub = testHelper.createStub(userModel, 'createUser');
                crerateUserStub.onCall(0).callsFake(() => Promise.resolve({
                    _id: '5c361f6214e6cd3932f68de8',
                    email: loginObject.email,
                    user_type: loginObject.userType,
                }));

                // call stub function of userModel
                const crerateUserSessionStub = testHelper.createStub(userModel, 'crerateUserSession');
                crerateUserSessionStub.onCall(0).callsFake(() => Promise.resolve(1));

                // call stub function of userModel
                const crerateStudentStub = testHelper.createStub(studentModel, 'createStudnet');
                crerateStudentStub.onCall(0).callsFake(() => Promise.resolve({
                    _id: '5c361f6214e6cd3932f68de8',
                    email: loginObject.email,
                    user_type: loginObject.userType,

                }));

                let res = await testHelper.chaiPostRequest(testData.uri.login, loginObject, 'form');
                testHelper.checkValidOutput(res);
                expect(res).to.have.status(responseService.getCode().codes.OK[0]);
                expect(res.body.message).to.eql(messages.LOGIN_SUCCESS);
                userModelStub.restore();
                crerateUserStub.restore();
                crerateUserSessionStub.restore();
                crerateStudentStub.restore();
            } catch (err) {
                testHelper.checkValidOutput(err);
                expect(err).to.have.status(responseService.getCode().codes.OK[0]);
            }
        });
    });

   
});
