const userController = require('./user.controller');
const {
    validate,
    checkValidationResult,
    checkIds,
    dynamicValidation,
} = require('./user.validator');
const jwtMiddleware = require('../../middlewares/jwt');

class UserRoute {
    constructor(app) {
        this.app = app;
        this.initRoutes();
    }

    initRoutes() {
      

        this.app.post(
            '/api/v1/user/login/github',
            [
                validate('loginEmail'),
                // jwtMiddleware.verifyJWT,
                checkValidationResult,
                dynamicValidation,
            ],
            (req, res, next) => {
                userController.loginEmail(req, res, next);
            },
        );
       
        this.app.patch(
            '/api/v1/user/:id',
            [
                jwtMiddleware.verifyJWT,
                checkIds,
            ],
            (req, res, next) => {
                userController.updateUser(req, res, next);
            },
        );

        this.app.get(
            '/api/v1/user/:id',
            [
                jwtMiddleware.verifyJWT,
                checkIds,
            ],
            (req, res, next) => {
                userController.getUser(req, res, next);
            },
        );

      
        // this.app.put(
        //     '/api/v1/user/logout',
        //     [
        //         validate('authorization'),
        //         checkValidationResult,
        //     ],
        //     (req, res, next) => {
        //         userController.logout(req, res, next);
        //     },
        // );

        // this.app.get(
        //     '/api/v1/userprofile',
        //     [
        //         validate('authorization'),
        //         checkValidationResult,
        //         jwtMiddleware.verifyJWT,
        //     ],
        //     (req, res, next) => {
        //         userController.userProfile(req, res, next);
        //     },
        // );

        this.app.post(
            '/api/v1/user/update-status',
            [
                validate('store'),
                // checkValidationResult,
                // jwtMiddleware.verifyJWT,
                // jwtMiddleware.checkStudentRole,
            ],
            async (req, res, next) => {
                await userController.changeStatus(req, res, next);
            },
        );
        this.app.post(
            '/api/v1/user/update-notification',
            [
                validate('store'),
                // checkValidationResult,
                // jwtMiddleware.verifyJWT,
                // jwtMiddleware.checkStudentRole,
            ],
            async (req, res, next) => {
                await userController.updateNotification(req, res, next);
            },
        );
    }
}

module.exports = UserRoute;
