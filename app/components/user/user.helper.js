/* eslint-disable no-underscore-dangle */

const nodemailer = require('nodemailer');
const userModel = require('./user.model');
const logger = require('../../utils/logging');
const UserDB = require('../../models/userModel');
const UserMetaDB = require('../../models/userMetaModel');

const SkillDB = require('../../models/skillModel');
const languageDB = require('../../models/languageModel');
const degreeDB = require('../../models/degreeModel');
const industryDB = require('../../models/industryModel');
const crypto = require('crypto');

const config = require('../../../config');
const jwtMiddleware = require('../../middlewares/jwt');
const jwt = require('jsonwebtoken');

module.exports = {
    createSession: async (userId, email, deviceType, deviceToken, sessionId) => new Promise(async (resolve, reject) => {
        try {
            // store session id in user_device table
            const sessionObj = {
                userId,
                session_id: sessionId,
                session_timeout: config.serverConfig.getTokenExpirationTime(),
                stay_logged_in: false,
                device_type: deviceType,
                device_token: deviceToken,
            };

            await userModel.crerateUserSession(sessionObj);

            // Generate JWT
            const authToken = await jwtMiddleware.generateJWT(userId, email, sessionId);

            resolve(authToken);
        } catch (err) {
            logger.error(`error during create session ${err}`);
            reject(err);
        }
    }),

    isUserExist: async (email) => {
        try {
            const user = await userModel.findUser({
                email,
            });
            if (user && user.email === email) {
                return true;
            }
            return false;
        } catch (error) {
            logger.error(error);
            return true;
        }
    },

    encryptPassword: async (pass) => {
        try {
            // const h = await bcrypt.hash(pass, saltRounds);
            // const genSalt = await bcrypt.genSalt(saltRounds);
            // const h = bcrypt.hash(pass, genSalt);
            const salt = await crypto.randomBytes(16).toString('hex');
            const hash = await crypto.pbkdf2Sync(pass, salt, 10000, 512, 'sha512').toString('hex');
            const o = {
                salt,
                hash,
            };
            return o;
        } catch (err) {
            return err;
        }
    },

    decryptpass: async (pass, hash, salt) => {
        try {
            // const match = await bcrypt.compare(pass, hash);

            const h = crypto.pbkdf2Sync(pass, salt, 10000, 512, 'sha512').toString('hex');
            return h === hash;
        } catch (err) {
            return err;
        }
    },

    calculateProfileLevel: async (u) => {
        logger.info('Inside calculateProfileLevel');
        try {
            // let u = await UserMetaDB.find({ userId: userId });
            // u = u[0];

            if (u.name && u.age && u.gender.toString() && u.city && u.skills && u.languages && u.degree && u.industry && u.occupation && u.title && u.company) {
                return 5;
            } else if (u.name && u.age && u.gender.toString() && u.city && u.skills && u.languages && u.degree && u.industry) {
                return 4;
            } else if (u.name && u.age && u.gender.toString() && u.city && u.skills) {
                return 3;
            } else if (u.name && u.age && u.gender.toString() && u.city) {
                return 2;
            } else if (u.name && u.age && u.gender.toString()) {
                return 1;
            }
            return 0;
        } catch (error) {
            logger.error(error);
            return error;
        }
    },

    sendMail: (emailId, name = 'sample', subject, body) => new Promise((resolve, reject) => {
        logger.info('Inside sendMail...');

        // Email send accound
        const smtpTransport = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: config.serverConfig.getSMTPEMAIL(),
                pass: config.serverConfig.getSMTPPASS(),
            },
        });

        // Email content and basic info
        const mailOptions = {
            from: `API test Topic <${config.serverConfig.getSMTPEMAIL()}>`, // sender address
            to: emailId, // list of receivers
            subject, // Subject line
            html: `Hello, ${name}! <br>${body}</b>`, // html body
        };

        // Send Email
        smtpTransport.sendMail(mailOptions)
            .then((response) => {
                logger.info(`Message sent: ${response.message}`);
                smtpTransport.close();
                resolve(response);
            })
            .catch((err) => {
                logger.error('Error in sendMail...', err);
                smtpTransport.close();
                reject(err);
            });
    }),

    sendVerificationMail: emailId => new Promise((resolve, reject) => {
        logger.info('Inside sendMail...');


        // Email send accound
        const smtpTransport = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: config.serverConfig.getSMTPEMAIL(),
                pass: config.serverConfig.getSMTPPASS(),
            },
        });

        // Send Email
        jwtMiddleware.generateEmailjwt(emailId)
            .then((foundToken) => {
                // Email content and basic info
                const mailOptions = {
                    from: `API test Topic <${process.env.SMTP_USERNAME}>`, // sender address
                    to: emailId, // list of receivers
                    subject: 'Verify Email', // Subject line
                    html: `Click here to verify : ${config.serverConfig.getBaseUrl()}/user/verifyEmail/${foundToken}`, // html body
                };
                return smtpTransport.sendMail(mailOptions);
            })

            .then((response) => {
                logger.info(`Message sent: ${response.message}`);
                smtpTransport.close();
                resolve(response);
            })
            .catch((err) => {
                logger.error('Error in sendMail...', err);
                smtpTransport.close();
                reject(err);
            });
    }),

    isExist: (mathcObject, Model) => {
        const exist = Model.findOne(mathcObject);
        if (exist && exist.length > 0) {
            return exist;
        }
        return false;
    },

    getIds: async (array, Model) => {
        const mappedIds = await array.map(async (e) => {
            const foundData = await Model.findOne({
                name: e,
            });
            return foundData._id;
        });
        const arrayToReturn = await Promise.all(mappedIds);
        return arrayToReturn; // Array of ids
    },

    updateUser: async (id, body) => {
        const user = body;
        try {
            // if (user.skills) {
            //     const skillIds = await module.exports.getIds(user.skills, SkillDB);
            //     user.skills = skillIds;
            // }
            // if (user.languages) {
            //     const languageIds = await module.exports.getIds(user.languages, languageDB);
            //     user.languages = languageIds;
            // }
            // if (user.degree) {
            //     const degreeIds = await module.exports.getIds(user.degree, degreeDB);
            //     user.degree = degreeIds;
            // }
            // if (user.industry) {
            //     const industryIds = await module.exports.getIds(user.industry, industryDB);
            //     user.industry = industryIds;
            // }
            const updatedUser = await UserMetaDB.findOneAndUpdate({
                userId: id,
            }, {
                $set: user,
            }, {
                new: true,
            });

            return updatedUser;
        } catch (error) {
            return error;
        }
    },
};
