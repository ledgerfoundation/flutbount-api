/* eslint-disable no-undef,no-unused-vars */


const {
    check,
    validationResult,
} = require('express-validator/check');
const {
    matchedData,
    sanitize,
} = require('express-validator/filter');
const responseService = require('../../utils/ResponseService');
const UserDB = require('../../models/userModel');
const messages = require('../../utils/messages');
const constants = require('../../utils/constants');
const jwtMiddleware = require('../../middlewares/jwt');


exports.checkValidationResult = (req, res, next) => {
    const result = validationResult(req);
    if (!result.isEmpty()) {
        responseService.send(res, {
            status: responseService.getCode().codes.BAD_REQUEST,
            data: {},
            message: result.array()[0].msg,
        });
    } else {
        next();
    }
};


exports.validate = (method) => {
    switch (method) {
    case 'loginGoogle':
        return [
            check('name')
                .isLength({
                    min: 1,
                })
                .withMessage(messages.checkIfRequired('name')),
            check('profileImage')
                .isLength({
                    min: 1,
                })
                .withMessage(messages.checkIfRequired('profileImage')),
            check('email')
                .isLength({
                    min: 1,
                })
                .withMessage(messages.checkIfRequired('Email'))
                .isEmail()
                .withMessage(messages.checkIfEmail('Email')),
            check('googleId')
                .isLength({
                    min: 1,
                })
                .withMessage(messages.checkIfRequired('Google Id')),
            check('deviceType')
                .isLength({
                    min: 1,
                })
                .withMessage(messages.checkIfRequired('Device type'))
                .isIn([
                    constants.deviceType.android,
                    constants.deviceType.iOS,
                    constants.deviceType.web,
                ])
                .withMessage(messages.checkIfValidValue('Device type')),
            check('deviceToken')
                .isLength({
                    min: 1,
                })
                .withMessage(messages.checkIfRequired('Device Token')),
        ];
    default:
        return [];
    }
};

exports.checkIds = (req, res, next) => {
    const jwtUser = req.userId;
    const paramId = req.params.id;
    if (jwtUser !== paramId) {
        return responseService.send(res, {
            status: responseService.getCode().codes.UNATHORISED_ACCESS,
            message: messages.UNAUTHORIZED_ACCESS,
        });
    }
    return next();
};
