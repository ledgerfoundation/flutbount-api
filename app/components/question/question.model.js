/* eslint-disable no-underscore-dangle */


const QuestionOD = require('../../models/questionODModel');
const logger = require('../../utils/logging');

module.exports = {
    findQuestionOD: async (whereArr = {}, populate = 'catergory') => new Promise(async (resolve, reject) => {
        try {
            const questionDetails = await QuestionOD
                .find(whereArr)
                .populate({
                    path: 'category',
                    // Get category- populate
                    model: 'category',
                    select: 'name',
                })
                .populate('student', {
                    first_name: 1,
                    last_name: 1,
                })
                .populate('tutor', {
                    first_name: 1,
                    last_name: 1,
                })
                .lean();
            resolve(questionDetails);
        } catch (err) {
            logger.error(`error during find userDetails ${err.messages}`);
            reject(err);
        }
    }),
    findQuestionAllOD: async () => new Promise(async (resolve, reject) => {
        try {
            const questionList = await QuestionOD.find({});
            logger.info('question list found!');
            resolve(questionList);
        } catch (err) {
            logger.error(`error during find userDetails ${err.messages}`);
            reject(err);
        }
    }),
    createQuestionOD: async (questionData = {}) => new Promise(async (resolve, reject) => {
        try {
            // console.log(questionData);
            const createQuestion = new QuestionOD(questionData);
            await createQuestion.save();
            resolve(createQuestion);
        } catch (err) {
            logger.error(`Error during create question: ${err.message}`);
            reject(err);
        }
    }),
};
