/* eslint-disable no-undef,no-unused-vars */


const { check, validationResult } = require('express-validator/check');
const { matchedData, sanitize } = require('express-validator/filter');
const responseService = require('../../utils/ResponseService');
const messages = require('../../utils/messages');
const constants = require('../../utils/constants');

exports.checkValidationResult = (req, res, next) => {
    const result = validationResult(req);
    if (!result.isEmpty()) {
        responseService.send(res, {
            status: responseService.getCode().codes.BAD_REQUEST,
            data: {},
            message: result.array()[0].msg,
        });
    } else {
        next();
    }
};

exports.validate = (method) => {
    switch (method) {
    case 'store':
        return [
            check('title')
                .isLength({ min: 1 })
                .withMessage(messages.checkIfRequired('Title')),
            check('category')
                .isLength({ min: 1 })
                .withMessage(messages.checkIfRequired('category')),
            check('queOfTheDate')
                .isAfter(new Date().toDateString())
                .withMessage('Date should be greater than yesterday'),
        ];
    default:
        return [];
    }
};

exports.checkDate = (req, res, next) => {
    const date = req.body.queOfTheDate;
    const today = new Date();
    const todayIso = today.toISOString();
    if (date >= todayIso) {
        return next();
    }
    return responseService.send(res, {
        status: responseService.getCode().codes.BAD_REQUEST,
        data: {},
        message: 'Date should be greater than or equal to today',
    });
};
