const questionController = require('./question.controller');
const { validate, checkValidationResult, checkDate } = require('./question.validator');
const jwtMiddleware = require('../../middlewares/jwt');

class QuestionRoute {
    constructor(app) {
        this.app = app;
        this.initRoutes();
    }

    initRoutes() {
        this.app.post(
            '/api/v1/questionOD',
            [
                validate('store'),
                checkValidationResult,
                checkDate,
                // jwtMiddleware.verifyJWT,
            ],
            async (req, res, next) => {
                await questionController.postQuestionOD(req, res, next);
            },
        );

        this.app.get(
            '/api/v1/questionOD/:id',
            [
                // jwtMiddleware.verifyJWT,

            ],
            (req, res, next) => {
                questionController.getQuestionOD(req, res, next);
            },
        );

        this.app.patch(
            '/api/v1/questionOD/:id',
            [
                // jwtMiddleware.verifyJWT,

            ],
            (req, res, next) => {
                questionController.updateQuestionOD(req, res, next);
            },
        );

        this.app.post(
            '/api/v1/questionOD/:id/answer',
            [
                jwtMiddleware.verifyJWT,

            ],
            (req, res, next) => {
                questionController.submitAns(req, res, next);
            },
        );
    }
}

module.exports = QuestionRoute;
