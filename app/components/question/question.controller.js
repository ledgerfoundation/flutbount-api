/* eslint-disable no-underscore-dangle */
const responseService = require('../../utils/ResponseService');
const logger = require('../../utils/logging');
const messages = require('../../utils/messages');
const questionModel = require('./question.model');
const QODModel = require('../../models/questionODModel');
const constants = require('../../utils/constants');
const helpers = require('../../utils/helper');
const awsFileUpload = require('../../utils/awsFileUpload');
const config = require('../../../config');

exports.postQuestionOD = async function (req, res) {
    const {
        title, category, options, rightAns, adminID, ansType, queOfTheDate,
    } = req.body;
    // let imageName = '';
    try {
        // upload  in aws s3
        // if (req.files) {
        //     // logger.error('Question file is mission!');
        //     // responseService.send(res, {
        //     //     status: responseService.getCode().codes.UNPROCESSED,
        //     //     message: messages.checkIfRequired('Question image'),
        //     // });
        //     // return;
        //     // validate image
        //     const imageExtension = req.files.questionImage.name.substr((req.files.questionImage.name.lastIndexOf('.') + 1));

        //     // validate image extension and mime type and max_size
        //     if (!constants.UPLOAD_IMAGE_EXTENSIONS.includes(imageExtension)) {
        //         logger.warn('Question image\'s extension is misconfigured.');
        //         responseService.send(res, {
        //             status: responseService.getCode().codes.UNPROCESSED,
        //             message: messages.FILE_TYPE_MISMATCH('Question image'),
        //         });
        //         return;
        //     } else if (!constants.UPLOAD_IMAGE_TYPE.includes(req.files.questionImage.mimetype)) {
        //         logger.warn('Question image\'s mimetype is misconfigured.');
        //         responseService.send(res, {
        //             status: responseService.getCode().codes.UNPROCESSED,
        //             message: messages.FILE_TYPE_MISMATCH('Question image'),
        //         });
        //         return;
        //     } else if (req.files.questionImage.data.toString().length > (constants.MAX_IMAGE_SIZE * 1000000)) {
        //         logger.warn(`Question image file is larger than ${constants.MAX_IMAGE_SIZE} MBs.`);
        //         responseService.send(res, {
        //             status: responseService.getCode().codes.UNPROCESSED,
        //             message: messages.HIGH_FILE_SIZE('Question image', constants.MAX_IMAGE_SIZE),
        //         });
        //         return;
        //     }

        //     // rename file name
        //     const imageRandNum = helpers.getRandomInt(1000, 9999);
        //     imageName = `${imageRandNum}${Date.now()}.${imageExtension}`;

        //     // upload file to s3
        //     try {
        //         const AwsFileUpload = await awsFileUpload.uploadFile(
        //             imageName, req.files.questionImage.data,
        //             (`${config.aws.getBucketName()}/question_media`), true,
        //         );

        //         imageName = `question_media/${imageName}`;
        //     } catch (err) {
        //         logger.error('Error while upload question image: ', err.message);
        //         responseService.send(res, {
        //             status: responseService.getCode().codes.INTERNAL_SERVER_ERROR,
        //             message: messages.FILE_UPLOAD_ERROR,
        //         });
        //         return;
        //     }
        // }

        // create quection from details
        const questionODObj = {
            title,
            isActive: true,
            category,
            questionImage: '',
            options,
            rightAns,
            adminID,
            ansType,
            queOfTheDate,
        };

        try {
            // create question
            const quectionOD = await questionModel.createQuestionOD(questionODObj);

            logger.info('question post successfully');
            responseService.send(res, {
                status: responseService.getCode().codes.OK,
                message: messages.QUESTION_POST,
                data: quectionOD,
            });
        } catch (err) {
            logger.error(`Error during post question: ${err}`);
            responseService.send(res, {
                status: responseService.getCode().codes.INTERNAL_SERVER_ERROR,
                message: messages.QUESTION_POST_ERROR,
                data: err,
            });
            return;
        }
    } catch (err) {
        logger.error(`Error during post question: ${err}`);
        responseService.send(res, {
            status: responseService.getCode().codes.INTERNAL_SERVER_ERROR,
            message: messages.QUESTION_POST_ERROR,
            data: {},
        });
    }
};

exports.getQuestionOD = async function (req, res) {
    try {
        const id = req.params.id;
        const questionOD = await QODModel.findById(id);
        if (!questionOD && questionOD.length < 1) {
            return responseService.send(res, {
                status: responseService.getCode().codes.BAD_REQUEST,
                message: 'No que found',
            });
        }
        return responseService.send(res, {
            status: responseService.getCode().codes.OK,
            message: messages.QUESTION_DETAILS,
            data: questionOD,
        });
    } catch (err) {
        logger.error(`Error during get question: ${err}`);
        return responseService.send(res, {
            status: responseService.getCode().codes.INTERNAL_SERVER_ERROR,
            message: messages.SERVER_ERROR_MESSAGE,
            data: { err },
        });
    }
};


exports.updateQuestionOD = async function (req, res) {
    try {
        const id = req.params.id;
        const questionOD = await QODModel.findByIdAndUpdate(id, { $set: req.body }, { new: true });
        if (!questionOD && questionOD.length < 1) {
            return responseService.send(res, {
                status: responseService.getCode().codes.BAD_REQUEST,
                message: 'No que found',
            });
        }
        return responseService.send(res, {
            status: responseService.getCode().codes.OK,
            message: messages.QUESTION_DETAILS,
            data: questionOD,
        });
    } catch (err) {
        logger.error(`Error during get question: ${err}`);
        return responseService.send(res, {
            status: responseService.getCode().codes.INTERNAL_SERVER_ERROR,
            message: messages.SERVER_ERROR_MESSAGE,
            data: { err },
        });
    }
};

exports.submitAns = async (req, res) => {
    try {
        const id = req.params.id;
        const objectToPush = {
            userID: req.userId,
            optionID: req.body.optionID,
        };

        const foundQue = await QODModel.findByIdAndUpdate(id, { $addToSet: { asnwers: objectToPush } }, { new: true });
        return responseService.send(res, {
            status: responseService.getCode().codes.CREATED,
            message: messages.ANSWER_SUBMITTED,
            data: foundQue,
        });
    } catch (error) {
        logger.error(`Error during submitting answer: ${error}`);
        return responseService.send(res, {
            status: responseService.getCode().codes.INTERNAL_SERVER_ERROR,
            message: messages.SERVER_ERROR_MESSAGE,
            data: { error },
        });
    }
};
