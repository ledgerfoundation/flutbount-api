const chai = require('chai');
const expect = chai.expect;
const constants = require('../../utils/constants');
const responseService = require('../../utils/ResponseService');
const testData = require('../../../config/testing/testData');
const testHelper = require('../../../tests/helperTest');
const messages = require('../../utils/messages');
// require mock dependencies
const studentModel = require('../student/student.model');
const userModel = require('../user/user.model');
const tutorModel = require('../tutor/tutor.model');
const questionModel = require('./question.model');
const answerRequestModel = require('../answer/answerRequest.model');

// Test cases for Question modules
describe('Test cases for Question modules', () => {

    describe('post question API', async () => {
        let questionObject = {};
        let studentJwtToken = '';
        let tutorJwtToken = '';
        beforeEach(() => {
            questionObject = {
                title: 'title1',
                category: testData.userId
            };
        });

        before(async () => {
            studentJwtToken = await testHelper.generateTestJWT(
                testData.userId,
                testData.email,
                constants.userType['student'],
                testData.session_id
            );

            tutorJwtToken = await testHelper.generateTestJWT(
                testData.userId,
                testData.email,
                constants.userType['tutor'],
                testData.session_id
            );
        });

        afterEach(() => {
            questionObject = {};
        });
        // send empty body in request
        testHelper.checkEmptyBody(testData.uri.postQuestion, 'post', {}, 'form');

        // send empty authorization in request
        testHelper.checkEmptyKey(testData.uri.postQuestion, 'post', questionObject, 'form', 'authorization', '', 'authorization');

        // send empty title in request
        testHelper.checkEmptyKey(testData.uri.postQuestion, 'post', questionObject, 'form', 'authorization', studentJwtToken, 'title');

        // send empty description in request
        testHelper.checkEmptyKey(testData.uri.postQuestion, 'post', questionObject, 'form', 'authorization', studentJwtToken, 'category');

        // Generate Error on studentModel.findByOneByObjectId
        it('Generate error on studentModel.findByOneByObjectId should have status code 500', async () => {
            try {
                // call stub function of findUserSession
                const findUserSessionStub = testHelper.createStub(userModel, 'findUserSession');
                findUserSessionStub.onCall(0)
                    .callsFake(() => Promise.resolve([{}]));

                // call stub function of findByOneByObjectId
                const studentModelStub = testHelper.createStub(studentModel, 'findByOneByObjectId');
                studentModelStub.onCall(0)
                    .callsFake(() => Promise.reject(1));

                let res = await testHelper.chaiPostRequest(testData.uri.postQuestion, questionObject, 'form', 'authorization', studentJwtToken);

                testHelper.checkValidOutput(res);
                expect(res)
                    .to
                    .have
                    .status(responseService.getCode().codes.INTERNAL_SERVER_ERROR[0]);
                expect(res.body.message)
                    .to
                    .eql(messages.QUESTION_POST_ERROR);
                findUserSessionStub.restore();
                studentModelStub.restore();
            } catch (err) {
                testHelper.checkValidOutput(err);
                expect(err)
                    .to
                    .have
                    .status(responseService.getCode().codes.INTERNAL_SERVER_ERROR[0]);
            }
        });

        // Generate Error on questionModel.createQuestion
        it('Generate error on questionModel.createQuestion should have status code 500', async () => {
            try {
                // call stub function of findUserSession
                const findUserSessionStub = testHelper.createStub(userModel, 'findUserSession');
                findUserSessionStub.onCall(0)
                    .callsFake(() => Promise.resolve([{}]));

                // call stub function of findByOneByObjectId
                const studentModelStub = testHelper.createStub(studentModel, 'findByOneByObjectId');
                studentModelStub.onCall(0)
                    .callsFake(() => Promise.resolve({
                        _id: testData.userId,
                    }));

                // call stub function of createQuestion
                const questionModelStub = testHelper.createStub(questionModel, 'createQuestion');
                questionModelStub.onCall(0)
                    .callsFake(() => Promise.reject(0));

                let res = await testHelper.chaiPostRequest(testData.uri.postQuestion, questionObject, 'form', 'authorization', studentJwtToken);

                testHelper.checkValidOutput(res);
                expect(res)
                    .to
                    .have
                    .status(responseService.getCode().codes.INTERNAL_SERVER_ERROR[0]);
                expect(res.body.message)
                    .to
                    .eql(messages.QUESTION_POST_ERROR);
                findUserSessionStub.restore();
                studentModelStub.restore();
                questionModelStub.restore();
            } catch (err) {
                testHelper.checkValidOutput(err);
                expect(err)
                    .to
                    .have
                    .status(responseService.getCode().codes.INTERNAL_SERVER_ERROR[0]);
            }
        });

        // Generate Error on find all tutor
        it('Generate error on find all tutor should have status code 200', async () => {
            try {
                // call stub function of findUserSession
                const findUserSessionStub = testHelper.createStub(userModel, 'findUserSession');
                findUserSessionStub.onCall(0)
                    .callsFake(() => Promise.resolve([{}]));

                // call stub function of findByOneByObjectId
                const studentModelStub = testHelper.createStub(studentModel, 'findByOneByObjectId');
                studentModelStub.onCall(0)
                    .callsFake(() => Promise.resolve({
                        _id: testData.userId,
                    }));

                // call stub function of createQuestion
                const questionModelStub = testHelper.createStub(questionModel, 'createQuestion');
                questionModelStub.onCall(0)
                    .callsFake(() => Promise.resolve({
                        question: 'question data goes here!'
                    }));

                // call stub function of findAllTutor
                const tutorModelStub = testHelper.createStub(tutorModel, 'findAllTutor');
                tutorModelStub.onCall(0)
                    .callsFake(() => Promise.reject(0));

                let res = await testHelper.chaiPostRequest(testData.uri.postQuestion, questionObject, 'form', 'authorization', studentJwtToken);

                testHelper.checkValidOutput(res);
                expect(res)
                    .to
                    .have
                    .status(responseService.getCode().codes.OK[0]);
                expect(res.body.message)
                    .to
                    .eql(messages.QUESTION_POST);
                findUserSessionStub.restore();
                studentModelStub.restore();
                questionModelStub.restore();
                tutorModelStub.restore();
            } catch (err) {
                testHelper.checkValidOutput(err);
                expect(err)
                    .to
                    .have
                    .status(responseService.getCode().codes.OK[0]);
            }
        });

        // Generate Error on create answer Request
        it('Generate error on find create answer Request should have status code 200', async () => {
            try {
                // call stub function of findUserSession
                const findUserSessionStub = testHelper.createStub(userModel, 'findUserSession');
                findUserSessionStub.onCall(0)
                    .callsFake(() => Promise.resolve([{}]));

                // call stub function of findByOneByObjectId
                const studentModelStub = testHelper.createStub(studentModel, 'findByOneByObjectId');
                studentModelStub.onCall(0)
                    .callsFake(() => Promise.resolve({
                        _id: testData.userId,
                    }));

                // call stub function of createQuestion
                const questionModelStub = testHelper.createStub(questionModel, 'createQuestion');
                questionModelStub.onCall(0)
                    .callsFake(() => Promise.resolve({
                        question: 'question data goes here!'
                    }));

                // call stub function of findAllTutor
                const tutorModelStub = testHelper.createStub(tutorModel, 'findAllTutor');
                tutorModelStub.onCall(0)
                    .callsFake(() => Promise.resolve([{
                        _id: testData.userId
                    }, {
                        _id: testData.session_id
                    }]));

                // call stub function of create answer Request
                const answerRequestModelStub = testHelper.createStub(answerRequestModel, 'createRequest');
                answerRequestModelStub.onCall(0)
                    .callsFake(() => Promise.reject(0));

                let res = await testHelper.chaiPostRequest(testData.uri.postQuestion, questionObject, 'form', 'authorization', studentJwtToken);

                testHelper.checkValidOutput(res);
                expect(res)
                    .to
                    .have
                    .status(responseService.getCode().codes.OK[0]);
                expect(res.body.message)
                    .to
                    .eql(messages.QUESTION_POST);
                findUserSessionStub.restore();
                studentModelStub.restore();
                questionModelStub.restore();
                tutorModelStub.restore();
                answerRequestModelStub.restore();
            } catch (err) {
                testHelper.checkValidOutput(err);
                expect(err)
                    .to
                    .have
                    .status(responseService.getCode().codes.OK[0]);
            }
        });

        // Send Valid Request
        it('Send Valid Request should have status code 200', async () => {
            try {
                // call stub function of findUserSession
                const findUserSessionStub = testHelper.createStub(userModel, 'findUserSession');
                findUserSessionStub.onCall(0)
                    .callsFake(() => Promise.resolve([{}]));

                // call stub function of findByOneByObjectId
                const studentModelStub = testHelper.createStub(studentModel, 'findByOneByObjectId');
                studentModelStub.onCall(0)
                    .callsFake(() => Promise.resolve({
                        _id: testData.userId,
                    }));

                // call stub function of createQuestion
                const questionModelStub = testHelper.createStub(questionModel, 'createQuestion');
                questionModelStub.onCall(0)
                    .callsFake(() => Promise.resolve({
                        question: 'question data goes here!'
                    }));

                // call stub function of findAllTutor
                const tutorModelStub = testHelper.createStub(tutorModel, 'findAllTutor');
                tutorModelStub.onCall(0)
                    .callsFake(() => Promise.resolve([{
                        _id: testData.userId
                    }, {
                        _id: testData.session_id
                    }]));

                // call stub function of create answer Request
                const answerRequestModelStub = testHelper.createStub(answerRequestModel, 'createRequest');
                answerRequestModelStub.onCall(0)
                    .callsFake(() => Promise.resolve(1));

                let res = await testHelper.chaiPostRequest(testData.uri.postQuestion, questionObject, 'form', 'authorization', studentJwtToken);

                testHelper.checkValidOutput(res);
                expect(res)
                    .to
                    .have
                    .status(responseService.getCode().codes.OK[0]);
                expect(res.body.message)
                    .to
                    .eql(messages.QUESTION_POST);
                findUserSessionStub.restore();
                studentModelStub.restore();
                questionModelStub.restore();
                tutorModelStub.restore();
                answerRequestModelStub.restore();
            } catch (err) {
                testHelper.checkValidOutput(err);
                expect(err)
                    .to
                    .have
                    .status(responseService.getCode().codes.OK[0]);
            }
        });
    });

    describe('get question API', async () => {
        let questionObject = {};
        let studentJwtToken = '';
        let tutorJwtToken = '';
        beforeEach(() => {});

        before(async () => {
            studentJwtToken = await testHelper.generateTestJWT(
                testData.userId,
                testData.email,
                constants.userType['student'],
                testData.session_id
            );

            tutorJwtToken = await testHelper.generateTestJWT(
                testData.userId,
                testData.email,
                constants.userType['tutor'],
                testData.session_id
            );
        });

        afterEach(() => {});
        // send empty body in request
        testHelper.checkEmptyBody(testData.uri.postQuestion, 'get', {}, 'form', 'authorization', studentJwtToken);

        // send empty authorization in request
        testHelper.checkEmptyKey(testData.uri.postQuestion, 'get', questionObject, 'form', 'authorization', '', 'authorization');

        // Send an invalid JWT
        it('Send an invalid JWT should have status code 401', async () => {
            try {

                let res = await testHelper.chaiGetRequest(testData.uri.postQuestion, questionObject, 'authorization', tutorJwtToken);

                testHelper.checkValidOutput(res);
                expect(res)
                    .to
                    .have
                    .status(responseService.getCode().codes.UNAUTHORIZED[0]);
                expect(res.body.message)
                    .to
                    .eql(messages.UNAUTHORIZED_ACCESS);
            } catch (err) {
                testHelper.checkValidOutput(err);
                expect(err)
                    .to
                    .have
                    .status(responseService.getCode().codes.UNAUTHORIZED[0]);
            }
        });

        // Generate Error on studentModel.findOne
        it('Generate error on studentModel.findOne should have status code 500', async () => {
            try {
                // call stub function of findUserSession
                const findUserSessionStub = testHelper.createStub(userModel, 'findUserSession');
                findUserSessionStub.onCall(0)
                    .callsFake(() => Promise.resolve([{}]));

                // call stub function of findByOneByObjectId
                const studentModelStub = testHelper.createStub(studentModel, 'findOne');
                studentModelStub.onCall(0)
                    .callsFake(() => Promise.reject(0));

                let res = await testHelper.chaiGetRequest(testData.uri.postQuestion, questionObject, 'authorization', studentJwtToken);

                testHelper.checkValidOutput(res);
                expect(res)
                    .to
                    .have
                    .status(responseService.getCode().codes.INTERNAL_SERVER_ERROR[0]);
                expect(res.body.message)
                    .to
                    .eql(messages.SERVER_ERROR_MESSAGE);
                findUserSessionStub.restore();
                studentModelStub.restore();
            } catch (err) {
                testHelper.checkValidOutput(err);
                expect(err)
                    .to
                    .have
                    .status(responseService.getCode().codes.INTERNAL_SERVER_ERROR[0]);
            }
        });

        // Generate Error on questionModel.findQuestion
        it('Generate error on questionModel.findQuestion should have status code 500', async () => {
            try {
                // call stub function of findUserSession
                const findUserSessionStub = testHelper.createStub(userModel, 'findUserSession');
                findUserSessionStub.onCall(0)
                    .callsFake(() => Promise.resolve([{}]));

                // call stub function of findOne
                const studentModelStub = testHelper.createStub(studentModel, 'findOne');
                studentModelStub.onCall(0)
                    .callsFake(() => Promise.resolve({
                        _id: testData.userId
                    }));

                // call stub function of findQuestion
                const questionModelStub = testHelper.createStub(questionModel, 'findQuestion');
                questionModelStub.onCall(0)
                    .callsFake(() => Promise.reject(0));

                let res = await testHelper.chaiGetRequest(testData.uri.postQuestion, questionObject, 'authorization', studentJwtToken);

                testHelper.checkValidOutput(res);
                expect(res)
                    .to
                    .have
                    .status(responseService.getCode().codes.INTERNAL_SERVER_ERROR[0]);
                expect(res.body.message)
                    .to
                    .eql(messages.SERVER_ERROR_MESSAGE);
                findUserSessionStub.restore();
                studentModelStub.restore();
                questionModelStub.restore();
            } catch (err) {
                testHelper.checkValidOutput(err);
                expect(err)
                    .to
                    .have
                    .status(responseService.getCode().codes.INTERNAL_SERVER_ERROR[0]);
            }
        });

        // Send Valid Request
        it('Send Valid Request should have status code 200', async () => {
            try {
                // call stub function of findUserSession
                const findUserSessionStub = testHelper.createStub(userModel, 'findUserSession');
                findUserSessionStub.onCall(0)
                    .callsFake(() => Promise.resolve([{}]));

                // call stub function of findOne
                const studentModelStub = testHelper.createStub(studentModel, 'findOne');
                studentModelStub.onCall(0)
                    .callsFake(() => Promise.resolve({
                        _id: testData.userId
                    }));

                // call stub function of findQuestion
                const questionModelStub = testHelper.createStub(questionModel, 'findQuestion');
                questionModelStub.onCall(0)
                    .callsFake(() => Promise.resolve(1));

                let res = await testHelper.chaiGetRequest(testData.uri.postQuestion, questionObject, 'authorization', studentJwtToken);

                testHelper.checkValidOutput(res);
                expect(res)
                    .to
                    .have
                    .status(responseService.getCode().codes.OK[0]);
                expect(res.body.message)
                    .to
                    .eql(messages.QUESTION_DETAILS);
                findUserSessionStub.restore();
                studentModelStub.restore();
                questionModelStub.restore();
            } catch (err) {
                testHelper.checkValidOutput(err);
                expect(err)
                    .to
                    .have
                    .status(responseService.getCode().codes.OK[0]);
            }
        });
    });

    describe('get question for tutors API', async () => {
        let questionObject = {};
        let studentJwtToken = '';
        let tutorJwtToken = '';
        beforeEach(() => {});

        before(async () => {
            studentJwtToken = await testHelper.generateTestJWT(
                testData.userId,
                testData.email,
                constants.userType['student'],
                testData.session_id
            );

            tutorJwtToken = await testHelper.generateTestJWT(
                testData.userId,
                testData.email,
                constants.userType['tutor'],
                testData.session_id
            );
        });

        afterEach(() => {});
        // send empty body in request
        testHelper.checkEmptyBody(testData.uri.getQuestionForTutor, 'get', {}, 'form', 'authorization', tutorJwtToken);

        // send empty authorization in request
        testHelper.checkEmptyKey(testData.uri.getQuestionForTutor, 'get', questionObject, 'form', 'authorization', '', 'authorization');

        // Send an invalid JWT
        it('Send an invalid JWT should have status code 401', async () => {
            try {

                let res = await testHelper.chaiGetRequest(testData.uri.getQuestionForTutor, questionObject, 'authorization', studentJwtToken);

                testHelper.checkValidOutput(res);
                expect(res)
                    .to
                    .have
                    .status(responseService.getCode().codes.UNAUTHORIZED[0]);
                expect(res.body.message)
                    .to
                    .eql(messages.UNAUTHORIZED_ACCESS);
            } catch (err) {
                testHelper.checkValidOutput(err);
                expect(err)
                    .to
                    .have
                    .status(responseService.getCode().codes.UNAUTHORIZED[0]);
            }
        });

        // Generate Error on tutorModel.findTutor
        it('Generate error on tutorModel.findTutor should have status code 500', async () => {
            try {
                // call stub function of findUserSession
                const findUserSessionStub = testHelper.createStub(userModel, 'findUserSession');
                findUserSessionStub.onCall(0)
                    .callsFake(() => Promise.resolve([{}]));

                // call stub function of findTutor
                const tutorModelStub = testHelper.createStub(tutorModel, 'findTutor');
                tutorModelStub.onCall(0)
                    .callsFake(() => Promise.reject(0));

                let res = await testHelper.chaiGetRequest(testData.uri.getQuestionForTutor, questionObject, 'authorization', tutorJwtToken);
                testHelper.checkValidOutput(res);
                expect(res)
                    .to
                    .have
                    .status(responseService.getCode().codes.INTERNAL_SERVER_ERROR[0]);
                expect(res.body.message)
                    .to
                    .eql(messages.SERVER_ERROR_MESSAGE);
                findUserSessionStub.restore();
                tutorModelStub.restore();
            } catch (err) {
                testHelper.checkValidOutput(err);
                expect(err)
                    .to
                    .have
                    .status(responseService.getCode().codes.INTERNAL_SERVER_ERROR[0]);
            }
        });

        // Generate Error on answerRequestModel.findQuestionAllTutor
        it('Generate error on answerRequestModel.findQuestionAllTutor should have status code 500', async () => {
            try {
                // call stub function of findUserSession
                const findUserSessionStub = testHelper.createStub(userModel, 'findUserSession');
                findUserSessionStub.onCall(0)
                    .callsFake(() => Promise.resolve([{}]));

                // call stub function of findTutor
                const tutorModelStub = testHelper.createStub(tutorModel, 'findTutor');
                tutorModelStub.onCall(0)
                    .callsFake(() => Promise.resolve({
                        _id: testData.userId
                    }));

                // call stub function of findQuestionAllTutor
                const answerRequestModelStub = testHelper.createStub(answerRequestModel, 'findQuestionAllTutor');
                answerRequestModelStub.onCall(0)
                    .callsFake(() => Promise.reject(0));

                let res = await testHelper.chaiGetRequest(testData.uri.getQuestionForTutor, questionObject, 'authorization', tutorJwtToken);
                testHelper.checkValidOutput(res);
                expect(res)
                    .to
                    .have
                    .status(responseService.getCode().codes.INTERNAL_SERVER_ERROR[0]);
                expect(res.body.message)
                    .to
                    .eql(messages.SERVER_ERROR_MESSAGE);
                findUserSessionStub.restore();
                tutorModelStub.restore();
                answerRequestModelStub.restore();
            } catch (err) {
                testHelper.checkValidOutput(err);
                expect(err)
                    .to
                    .have
                    .status(responseService.getCode().codes.INTERNAL_SERVER_ERROR[0]);
            }
        });

        // Send Valid Request
        it('Send Valid Request should have status code 200', async () => {
            try {
                // call stub function of findUserSession
                const findUserSessionStub = testHelper.createStub(userModel, 'findUserSession');
                findUserSessionStub.onCall(0)
                    .callsFake(() => Promise.resolve([{}]));

                // call stub function of findTutor
                const tutorModelStub = testHelper.createStub(tutorModel, 'findTutor');
                tutorModelStub.onCall(0)
                    .callsFake(() => Promise.resolve({
                        _id: testData.userId
                    }));

                // call stub function of findQuestionAllTutor
                const answerRequestModelStub = testHelper.createStub(answerRequestModel, 'findQuestionAllTutor');
                answerRequestModelStub.onCall(0)
                    .callsFake(() => Promise.resolve(1));

                let res = await testHelper.chaiGetRequest(testData.uri.getQuestionForTutor, questionObject, 'authorization', tutorJwtToken);

                testHelper.checkValidOutput(res);
                expect(res)
                    .to
                    .have
                    .status(responseService.getCode().codes.OK[0]);
                expect(res.body.message)
                    .to
                    .eql(messages.MODULE_LIST_SUCCESS('Question'));
                findUserSessionStub.restore();
                tutorModelStub.restore();
                answerRequestModelStub.restore();
            } catch (err) {
                testHelper.checkValidOutput(err);
                expect(err)
                    .to
                    .have
                    .status(responseService.getCode().codes.OK[0]);
            }
        });
    });
});
