require('dotenv').config();
const express = require('express');
const Components = require('./components/');
const Middlewares = require('./middlewares');
const DBConnection = require('./db');

class Server {
    constructor(config) {
        this.app = express();
        this.config = config;
        this.initializeApp();
        this.initializeDbConnection();
    }

    initializeDbConnection() {
        this.connection = new DBConnection(this.config);
    }

    initializeApp() {
        this.initConfig();
        this.setMiddlewares();
        this.setComponents();
    }

    initConfig() {
        this.port = this.config.serverConfig.getPort();
        this.app.set('port', this.port);
    }

    setMiddlewares() {
        this.middlewares = new Middlewares(this.app);
    }

    setComponents() {
        this.components = new Components(this.app);
    }

    startServer() {
        this.app.listen(this.app.get('port'), () => {
            console.log(`Server is running on port : ${this.port}`); // eslint-disable-line no-console
        });
        this.isServerUp();
    }

    isServerUp() {
         this.app.get(
            '/test',
            [
            //     jwtMiddleware.verifyJWT,
            //     jwtMiddleware.checkStudentRole,
            ],
            (req, res, next) => {
                res.sendFile(__dirname +'/index.html');
                // expertController.getFilteredExpert(req, res, next);
            },
        );


        this.app.get('/isServerUp', (req, res) => {
            res.send('Server is up');
        });
    }

    getApp() {
        return this.app;
    }
}

module.exports = Server;
