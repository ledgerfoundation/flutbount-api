const mongoose = require('mongoose');

class DbConnection {
    constructor(config) {
        this.config = config;
        this.initialize();
    }

    initialize() {
        const connectionString =
            `mongodb://${this.config.dbConfig.getUser()}:${this.config.dbConfig.getPassword()}@${this.config.dbConfig.getHost()}:${this.config.dbConfig.getPort()}/${this.config.dbConfig.getDatabase()}`;

        mongoose.connect(connectionString, {
            useNewUrlParser: true,
            useCreateIndex: true,
        });
    }
}

module.exports = DbConnection;
